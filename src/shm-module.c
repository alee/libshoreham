/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "shm-module.h"
#include "shm-internal.h"

struct _ShmModulePrivate
{
  GModule *lib; /* own: Variant widget library path read from the GSchema file */
};

#define VARIANT_LOAD_PATH LIBDIR "/variant/module/libshoreham-backend.so"
#define COMPAT_LOAD_PATH "/usr/lib/variant/module/libshoreham-backend.so"

#define SHM_MODULE_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), SHM_TYPE_MODULE, ShmModulePrivate))

G_DEFINE_TYPE (ShmModule, shm_module, G_TYPE_TYPE_MODULE);

static gboolean
shm_module_load (GTypeModule *gmodule)
{
  ShmModule *self = NULL;
  ShmModulePrivate *priv = NULL;
  gchar *loadable_path = NULL;
  GType
  (*register_type_vkeyboard) (GTypeModule *) = NULL;
  GType
  (*register_type_combo_box) (GTypeModule *) = NULL;

  g_assert (SHM_IS_MODULE(gmodule));

  self = SHM_MODULE(gmodule);
  priv = SHM_MODULE (gmodule)->priv;

  if (!priv->lib && !(priv->lib = g_module_open (VARIANT_LOAD_PATH, 0))
      && !(priv->lib = g_module_open (COMPAT_LOAD_PATH, 0)))
    {
      WARNING("Could not load library %s or %s (%s)]", VARIANT_LOAD_PATH,
              COMPAT_LOAD_PATH, g_module_error ());
      g_free (loadable_path);
      return FALSE;
    }


  if (g_module_symbol (priv->lib, "shm_vkeyboard_plugin_register_type",
                       (gpointer *) (void *) &register_type_vkeyboard)
      && register_type_vkeyboard)
    {
      GType module_type;
      if (!(module_type = register_type_vkeyboard (gmodule)))
        {
          WARNING("Could not register type for plugin");
          return FALSE;
        }
      else
        {
          self->vkeyboard = (ShmVKeyboard *) g_object_new (module_type, NULL);
          g_assert (self->vkeyboard != NULL);
          DEBUG(" module type name  %s", g_type_name (module_type));
        }
    }

  if (g_module_symbol (priv->lib, "shm_combo_box_plugin_register_type",
                      (gpointer *) (void *) &register_type_combo_box)
      && register_type_combo_box)
    {
      GType module_type;
      if (!(module_type = register_type_combo_box (gmodule)))
        {
          WARNING("Could not register type for plugin");
          return FALSE;
        }
      else
        {
          self->combo_box = (ShmComboBox *) g_object_new (module_type, NULL);
          g_assert (self->combo_box != NULL);
          DEBUG(" module type name  %s", g_type_name (module_type));
        }
    }

  return TRUE;
}

static void
shm_module_unload (GTypeModule *gmodule)
{
  ShmModulePrivate *priv = NULL;

  g_assert (SHM_IS_MODULE (gmodule));

  priv = SHM_MODULE (gmodule)->priv;
  g_module_close (priv->lib);
  priv->lib = NULL;
}

static void
shm_module_dispose (GObject *object)
{
  G_OBJECT_CLASS (shm_module_parent_class)->dispose (object);
}

static void
shm_module_finalize (GObject *object)
{
  g_assert (SHM_IS_MODULE (object));

  G_OBJECT_CLASS (shm_module_parent_class)->finalize (object);
}

static void
shm_module_class_init (ShmModuleClass *klass)
{
  GObjectClass *gobject_class = NULL;
  GTypeModuleClass *gmodule_class = NULL;

  g_assert (SHM_IS_MODULE_CLASS (klass));

  gobject_class = G_OBJECT_CLASS (klass);
  gobject_class->finalize = shm_module_finalize;
  gobject_class->dispose = shm_module_dispose;

  gmodule_class = G_TYPE_MODULE_CLASS (klass);
  gmodule_class->load = shm_module_load;
  gmodule_class->unload = shm_module_unload;
  g_type_class_add_private (gobject_class, sizeof(ShmModulePrivate));
}

static void
shm_module_init (ShmModule *self)
{
  g_assert (SHM_IS_MODULE (self));

  self->priv = SHM_MODULE_GET_PRIVATE (self);
  g_assert (self->priv != NULL);
}

ShmModule*
shm_module_get_default (void)
{
  static ShmModule *module = NULL;
  if (module == NULL)
    {
      module = g_object_new (SHM_TYPE_MODULE, NULL);
      if (!module || !g_type_module_use (G_TYPE_MODULE (module)))
        {
          g_warning ("Unable to load plugin module");
          return 0;
        }
    }
  return module;
}
