/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include "shm-vkeyboard.h"

#include "shm-internal.h"
#include "shm-module.h"

G_DEFINE_ABSTRACT_TYPE (ShmVKeyboard, shm_vkeyboard, G_TYPE_OBJECT);

#define SHM_VKEYBOARD_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), SHM_TYPE_VKEYBOARD, ShmVKeyboardPrivate))

typedef struct
{
  ShmVKeyboardConfirmCallback user_callback;
  gpointer user_data;
} ConfirmCbData;


static void
shm_vkeyboard_class_init (ShmVKeyboardClass *klass)
{
  g_assert (SHM_IS_VKEYBOARD_CLASS (klass));

  klass->show = NULL;
  klass->hide = NULL;
}

static void
shm_vkeyboard_init (ShmVKeyboard *self)
{
  g_assert (SHM_IS_VKEYBOARD (self));

  self->priv = SHM_VKEYBOARD_GET_PRIVATE (self);
}

static void
confirm_cb (gpointer user_data)
{
  ConfirmCbData *method_data = user_data;

  g_assert_nonnull (method_data);

  if (method_data->user_callback != NULL)
    method_data->user_callback (method_data->user_data);
  shm_vkeyboard_hide ();
  g_slice_free (ConfirmCbData, method_data);
}

/**
 * shm_vkeyboard_show:
 *
 * @default_text : default text to display or %NULL
 * @callback (scope async) (nullable) : a #ShmVKeyboardConfirmCallback called when the
 *                                     user select submit button for confirm text entry
 *                                     shown with shm_vkeyboard_show().
 * @user_data : (nullable) User data to be passed to the @callback
 *
 * it will show Virtual keyboard.
 */
void
shm_vkeyboard_show (const gchar *default_text,
                    ShmVKeyboardConfirmCallback callback,
                    gpointer user_data)
{
  ClutterActor *stage;
  G_GNUC_BEGIN_IGNORE_DEPRECATIONS;
  stage = clutter_stage_get_default ();
  G_GNUC_END_IGNORE_DEPRECATIONS;
  shm_vkeyboard_show_for_actor (stage, default_text, callback, user_data);
}

/**
 * shm_vkeyboard_show_for_actor:
 *
 * @actor : The actor for which the virtual keyboard will be displayed
 * @default_text : default text to display or %NULL
 * @callback (scope async) (nullable) : a #ShmVKeyboardConfirmCallback called when the
 *                                     user select submit button for confirm text entry
 *                                     shown with shm_vkeyboard_show().
 * @user_data : (nullable) User data to be passed to the @callback
 *
 * it will show Virtual keyboard.
 */
void
shm_vkeyboard_show_for_actor (ClutterActor *actor,
                              const gchar *default_text,
                              ShmVKeyboardConfirmCallback callback,
                              gpointer user_data)
{
  ShmVKeyboardClass *klass;
  ConfirmCbData *method_data;
  ShmModule *module = shm_module_get_default ();

  g_assert (SHM_IS_MODULE (module));
  g_assert (SHM_IS_VKEYBOARD (module->vkeyboard));
  g_assert (CLUTTER_IS_ACTOR (actor));

  DEBUG ("Default vkeyboard text '%s'", default_text);

  klass = SHM_VKEYBOARD_GET_CLASS (module->vkeyboard);
  g_assert (SHM_IS_VKEYBOARD_CLASS (klass));

  method_data = g_slice_new0 (ConfirmCbData);
  method_data->user_callback = callback;
  method_data->user_data = user_data;
  klass->show (module->vkeyboard, actor, default_text, confirm_cb, method_data);
}

/**
 * shm_vkeyboard_hide:
 *
 * it will hide virtual keyboard.
 */
void
shm_vkeyboard_hide (void)
{
  ShmVKeyboardClass *klass;
  ShmModule *module = shm_module_get_default ();

  g_assert (SHM_IS_MODULE (module));
  g_assert (SHM_IS_VKEYBOARD (module->vkeyboard));

  klass = SHM_VKEYBOARD_GET_CLASS (module->vkeyboard);
  g_assert (SHM_IS_VKEYBOARD_CLASS (klass));
  klass->hide (module->vkeyboard);
}
