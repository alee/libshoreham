/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "shm-combobox.h"
#include "shm-internal.h"
#include "shm-module.h"

G_DEFINE_ABSTRACT_TYPE (ShmComboBox, shm_combo_box, G_TYPE_OBJECT);

#define SHM_COMBO_BOX_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), SHM_TYPE_COMBO_BOX, ShmComboBoxPrivate))

typedef struct
{
  ShmComboBoxSelectCallback user_callback;
  gpointer user_data;
} SelectedCbData;


static void
shm_combo_box_class_init (ShmComboBoxClass *klass)
{
  g_assert (SHM_IS_COMBO_BOX_CLASS (klass));
  klass->show = NULL;
}

static void
shm_combo_box_init (ShmComboBox *self)
{
  g_assert (SHM_IS_COMBO_BOX (self));

  self->priv = SHM_COMBO_BOX_GET_PRIVATE (self);
}

static void
selected_cb (gint selected_index, gpointer user_data)
{
  SelectedCbData *method_data = user_data;

  g_assert_nonnull (method_data);

  if (method_data->user_callback != NULL)
    method_data->user_callback (selected_index, method_data->user_data);
  shm_combo_box_hide ();
  g_slice_free (SelectedCbData, method_data);
}

/**
 * shm_combo_box_show:
 *
 * @items : (transfer none) (element-type utf8) List items are used to populate combo box widget.
 * @selected_index : Current selected index.
 * @callback : (scope async) A #ShmComboBoxSelectCallback called when the user selects
 *                             an item in a combo box shown with shm_combo_show().
 * @user_data : (nullable) User data to be passed to the @callback
 *
 * It will show combo box widget.
 */
void
shm_combo_box_show (GList *items,
                    gint selected_index,
                    ShmComboBoxSelectCallback callback,
                    gpointer user_data)
{
  ClutterActor *stage;
  G_GNUC_BEGIN_IGNORE_DEPRECATIONS;
  stage = clutter_stage_get_default ();
  G_GNUC_END_IGNORE_DEPRECATIONS;
  shm_combo_box_show_for_actor (stage, items, selected_index, callback, user_data);
}

/**
 * shm_combo_box_show_for_actor:
 *
 * @actor : The actor for which the combobox will be displayed.
 * @items : (transfer none) (element-type utf8) List items are used to populate combo box widget.
 * @selected_index : Current selected index.
 * @callback : (scope async) A #ShmComboBoxSelectCallback called when the user selects
 *                             an item in a combo box shown with shm_combo_show().
 * @user_data : (nullable) User data to be passed to the @callback
 *
 * It will show combo box widget.
 */
void
shm_combo_box_show_for_actor (ClutterActor *actor,
                              GList *items,
                              gint selected_index,
                              ShmComboBoxSelectCallback callback,
                              gpointer user_data)
{
  ShmComboBoxClass *klass;
  GList *list_copy;
  SelectedCbData *method_data;
  ShmModule *module = shm_module_get_default ();

  g_assert (SHM_IS_MODULE (module));
  g_assert (SHM_IS_COMBO_BOX (module->combo_box));
  g_assert (CLUTTER_IS_ACTOR (actor));
  g_assert (items != NULL);

  DEBUG("Selected index %d", selected_index);
  klass = SHM_COMBO_BOX_GET_CLASS (module->combo_box);
  g_assert (SHM_IS_COMBO_BOX_CLASS (klass));

  list_copy = g_list_copy_deep (items, (GCopyFunc) g_strdup, NULL);

  method_data = g_slice_new0 (SelectedCbData);
  method_data->user_callback = callback;
  method_data->user_data = user_data;
  klass->show (module->combo_box, actor, list_copy, selected_index, selected_cb,
               method_data);
}

/**
 * shm_combo_box_hide:
 *
 * It will hide combo box widget
 */
void
shm_combo_box_hide (void)
{
  ShmComboBoxClass *klass;
  ShmModule *module = shm_module_get_default ();

  g_assert (SHM_IS_MODULE (module));
  g_assert (SHM_IS_COMBO_BOX (module->combo_box));

  klass = SHM_COMBO_BOX_GET_CLASS (module->combo_box);
  g_assert (SHM_IS_COMBO_BOX_CLASS (klass));
  klass->hide (module->combo_box);
}
