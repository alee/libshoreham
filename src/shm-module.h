/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SHM_MODULE_H_
#define SHM_MODULE_H_

#include <glib-object.h>
#include <glib.h>
#include <gmodule.h>
#include <gio/gio.h>

#include "shm-combobox.h"
#include "shm-vkeyboard.h"

#define SHM_TYPE_MODULE            (shm_module_get_type ())
#define SHM_MODULE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SHM_TYPE_MODULE, ShmModule))
#define SHM_MODULE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  SHM_TYPE_MODULE, ShmModuleClass))
#define SHM_IS_MODULE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SHM_TYPE_MODULE))
#define SHM_IS_MODULE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  SHM_TYPE_MODULE))
#define SHM_MODULE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  SHM_TYPE_MODULE, ShmModuleClass))

typedef struct _ShmModule ShmModule;
typedef struct _ShmModuleClass ShmModuleClass;
typedef struct _ShmModulePrivate ShmModulePrivate;

struct _ShmModule
{
  GTypeModule parent;

  ShmComboBox *combo_box; /* own : Module registered type object is Combo box*/
  ShmVKeyboard *vkeyboard; /* own : Module registered type object is virtual keyboard*/

  ShmModulePrivate *priv; /* own : Shm Module private structure*/
};

struct _ShmModuleClass
{
  GTypeModuleClass parent_class;
};

GType
shm_module_get_type (void);
ShmModule*
shm_module_get_default (void);

#endif /* SHM_MODULE_H_ */
