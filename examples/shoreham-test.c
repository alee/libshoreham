/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <webkit2/webkit2.h>
#include "../src/shoreham.h"
#include <string.h>

const char *html =
    "<html>"
        "<h1>This is a Test client for shoream library</h1>"
        "<p> Shoreham is a dynamically loadable variant widget library,"
        "it provides the generic interface to application or library to use these widget."
        " </br></br></br>Input box </br>"
        "<input type=\"text\" value=\"Tap on input box\"/>"
        "<div> </br> Combo Box Widget </br>"
        "<select>"
        "<option value=\"volvo\">Volvo</option>"
        "<option value=\"saab\">Saab</option>"
        "<option value=\"mercedes\">Mercedes</option>"
        "<option value=\"audi\">Audi</option>"
        "</select> </div>" ""
        "</html>";

static void
go_pressed (gpointer user_data)
{
  g_print ("%s", __FUNCTION__);
}

static void
combo_box_widget_select (gint selected_index,
                         gpointer user_data)
{
  g_print ("%s  Selected_index =%d", __FUNCTION__, selected_index);
}

static void
input_method_state_cb (WebKitWebClutterView *view,
                       bool enabled,
                       gdouble x,
                       gdouble y,
                       gdouble width,
                       gdouble height,
                       gpointer user_data)
{
  g_assert (view != NULL);
  g_print (" Input Box enabled %d", enabled);

  if(enabled)
    shm_vkeyboard_show_for_actor (CLUTTER_ACTOR (view), NULL, go_pressed, NULL);
  else
    shm_vkeyboard_hide ();
}

static void
dropdown_show_cb (WebKitWebClutterView *clutter_view,
                  WebKitPopupMenu *popup_menu,
                  gpointer user_data)
{
  GList *items = NULL;
  GSList *list;
  gchar *item_str = NULL;
  gchar *str = NULL;

  g_assert (WEBKIT_IS_WEB_CLUTTER_VIEW (clutter_view));
  g_assert (WEBKIT_IS_POPUP_MENU (popup_menu));

   for (list = webkit_popup_menu_get_items (popup_menu); list != NULL;
       list = list->next)
    {
      WebKitPopupMenuItem *item_popup = WEBKIT_POPUP_MENU_ITEM (list->data);
      str = (gchar *) webkit_popup_menu_item_get_text (item_popup);
      item_str = (gchar *)g_new (gchar *, 1);
      strcpy (item_str, str);
      items = g_list_append (items, item_str);
    }
  shm_combo_box_show_for_actor (CLUTTER_ACTOR (clutter_view),
                                items,
                                webkit_popup_menu_get_selected_id (popup_menu),
                                combo_box_widget_select,
                                NULL);
  g_list_free (items);
}

int
main (int argc, char **argv)
{
  ClutterActor *clutter_view = NULL;
  ClutterActor *stage;
  WebKitWebView *web_view;
  ClutterLayoutManager *vbox = NULL;

  if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
    {
      g_error ("Failed to initialized clutter.");
      return 1;
    }
  vbox = clutter_bin_layout_new (CLUTTER_BIN_ALIGNMENT_FILL,
                                 CLUTTER_BIN_ALIGNMENT_FILL);
  g_assert (vbox != NULL);
  stage = clutter_stage_new ();
  g_assert (stage != NULL);
  clutter_actor_set_size (stage, 800, 600);
  clutter_stage_set_user_resizable (CLUTTER_STAGE (stage), true);
  clutter_actor_set_background_color (stage, CLUTTER_COLOR_Black);
  clutter_actor_set_layout_manager (stage, vbox);

  clutter_view = CLUTTER_ACTOR (
      g_object_new (WEBKIT_TYPE_WEB_CLUTTER_VIEW, NULL));
  g_assert (WEBKIT_IS_WEB_CLUTTER_VIEW (clutter_view));
  clutter_actor_set_position (clutter_view, 0, 0);
  clutter_actor_set_size (clutter_view, 800, 600);
  clutter_actor_add_child (stage, clutter_view);

  web_view = webkit_web_clutter_view_get_web_view (
      WEBKIT_WEB_CLUTTER_VIEW (clutter_view));

  g_assert (web_view != NULL);
  clutter_actor_add_child (stage, clutter_view);
  webkit_web_view_load_html (web_view, html, "file://");
  clutter_actor_show (stage);
  g_signal_connect (clutter_view, "input-method-state-changed",
                    G_CALLBACK (input_method_state_cb), NULL);
  g_signal_connect (clutter_view, "show-popup-menu",
                    G_CALLBACK (dropdown_show_cb), NULL);

  clutter_main ();
  return 0;
}
