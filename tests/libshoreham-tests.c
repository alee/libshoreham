/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "shoreham.h"
#include "shm-module.h"
#include <webkit2/webkit2.h>

static void
go_pressed (gpointer user_data)
{
  g_assert (user_data != NULL);
}

static void
combo_box_widget_select (gint selected_index, gpointer user_data)
{
  g_assert (user_data != NULL);
}

static void
test_shm_vkeyboard_show (void)
{
  ClutterActor *stage = NULL,*view = NULL;

  stage = clutter_stage_new ();
  g_assert (CLUTTER_IS_ACTOR (stage));
  view = clutter_actor_new ();
  g_assert (CLUTTER_IS_ACTOR (view));
  clutter_actor_add_child (stage, view);
  shm_vkeyboard_show_for_actor (view, NULL, go_pressed, NULL);
}

static void
test_shm_vkeyboard_hide (void)
{
  shm_vkeyboard_hide ();
}

static void
test_shm_combo_box_show(void)
{
  ClutterActor *stage = NULL,*view = NULL;
  GList *items = NULL;
  gchar *item_str = NULL;
  gint i = 0;

  stage = clutter_stage_new ();
  g_assert (CLUTTER_IS_ACTOR (stage));
  view = clutter_actor_new ();
  g_assert (CLUTTER_IS_ACTOR (view));
  clutter_actor_add_child (stage, view);
  for (i = 0; i < 5; i++)
   {
     item_str = (gchar *)g_new (gchar *, 1);
     strcpy (item_str, "Test");
     items = g_list_append (items, item_str);
   }

  shm_combo_box_show_for_actor (view, items, 0, combo_box_widget_select, NULL);
  g_list_free (items);
}

static void
test_shm_combo_box_hide (void)
{
  shm_combo_box_hide ();
}

int
main (int argc, char **argv)
{
 g_test_init (&argc, &argv, NULL);

  if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
    {
      g_error ("Failed to initialized clutter.");
      return 1;
    }

  g_test_add_func ("/libshoreham-tests/test_shm_vkeyboard_show",
                   test_shm_vkeyboard_show);
  g_test_add_func ("/libshoreham-tests/test_shm_vkeyboard_hide",
                   test_shm_vkeyboard_hide);
  g_test_add_func ("/libshoreham-tests/test_shm_combo_box_show",
                   test_shm_combo_box_show);
  g_test_add_func ("/libshoreham-tests/test_shm_combo_box_hide",
                   test_shm_combo_box_hide);

  return g_test_run ();
}
